#!/usr/bin/env python
# Software License Agreement (BSD License)
# Author: Jan Balewski (jan.balewski@gmail.com)
# Updated July 2015

"""
 Example of using calibrated servo
"""

import time
import os
from servoDriver import ServoDriver 
import yaml
from pprint import pprint

#===============================
#=======    MAIN Program
#===============================
ymlName ="./setup/futubaS3003.servo.yml"

print 'reading servo config from :',ymlName
if not os.path.isfile(ymlName) :
    print ' not existing input YAML name, do nothing ???'
    exit(1)
armConf=yaml.load( open(ymlName, "r" ) )

print "activate servo comu with this params"
pprint(armConf)

arm = ServoDriver()
arm.setupController()
arm.config(armConf)

for ang in [15., 30., -20, 5.]:
    arm.setAngleDeg(ang)
    print 'set armAngDeg=%s, status=%s '%(ang,arm.status)
    time.sleep(1.)
    
print "servos STOP"
arm.fullStop()
