#!/usr/bin/python

# servoCalibratorMain.py
# ----------
# Calibrates standard servo : main
# input/output : yaml  with servo params
# Author: Jan Balewski (jan.balewski@gmail.com)
# Updated December 2016

from ServoCalibGUI import *
from optparse import OptionParser

#===============================
#=======   define   main_run()
#===============================
def main_run():
  
    rootWin = Tkinter.Tk()
    rootWin.title( 'Servo Calibrator GUI ver 1.2          jan.balewski@gmail.com' )
    rootWin.resizable( 0, 0 )

    app = ServoCalibGUI(rootWin,options)
    #print "====M: init completed ========"

    #..... recursive function updating GUI
    def update_gui():
        rootWin.after(10, update_gui)

    #  ... self-loop activated   
    update_gui()
  
    #..... emergency termination of controll app
    rootWin.protocol( 'WM_DELETE_WINDOW', app.actExit)
    try:
        app.actStart()
    except:
        app.actExit()


#===============================
#=======    MAIN Program
#===============================

if __name__ == '__main__':
    
    # -- command line options
    usage = 'usage: %prog [options] MUST run as gksu, add " ... "  around command w/ params'
    parser = OptionParser(usage)

    parser.add_option("-m","--mockServos", 
                      dest="MOCK_SERVOS", 
                      action="store_true", 
                      help="disable servos", 
                      default = False)
    
    parser.add_option("-v","--verbose", 
                      dest="VERBOSE", 
                      action = "store_true", 
                      help="print aux info", 
                      default = False)
 
    parser.add_option("-n","--name", 
                      dest="CONF_NAME", 
                      type="string",
                      help="servo setup (yml) name", 
                      default = "generic")

    parser.add_option("-s","--setupPath", 
                      dest="SETUP", 
                      type="string",
                      help="hardware configuration location", 
                      default = "./setup/")

    (options,args) = parser.parse_args()


    if options.VERBOSE:
        print ">>> Executing with parameters:"
        print ">>> NAME  = %s" % options.CONF_NAME 
        print ">>> setup PATH  = %s" % options.SETUP
        print ">>> SERVOS  = %s" % (not options.MOCK_SERVOS) # True = On, False = Off
 

    #################################
    #  MAIN
    #################################

    main_run()
