# Author: Jan Balewski (jan.balewski@gmail.com)
# Software License Agreement (BSD License)
# Updated : December, 2016

"""
Drives many servo motors (seMo)  PCA9685, channel is 0-15
pwmWidth is 150-600 (for 12-bit resolution , max=4096)
Special cases:
      pwmWidth=0 - power-off given servo
      chan=99 - power off all servos

mult-servo cpmmand is passed as SeMoMulti.srv
* command: {chan,pulseWidth,delay_ms}xN - dynamic vector
* response uint64 - meaning TBD

that is received on subscribed topic: 'seMoMulti'

Dependencies:
* Adafruit_Python_PCA9685
* pigpiod (for sudo free access to GPIO)
* malina/common/rpiUtils - interface  adafruit to I2C
"""

'''
This code requires  PCA9685 on I2C bus 1 addr 0x40
To check I2C address  for servo-16 is 0x40:
sudo i2cdetect -y 1
'''

import os
import sys 
import time
import math
from math import pi as PI

sys.path.append('/home/balewski/malina/common/')
from rpiUtils import *
from I2c_for_Adafruit import *

# Import the PCA9685 module.
sys.path.append('/home/balewski/Adafruit_Python_PCA9685/Adafruit_PCA9685')
from PCA9685 import  PCA9685

#==================================  
class ServoDriver():
    def __init__(self, dbg=0):
        self.dbg=dbg
        isRPi()

    def setupController(self):
        pwm_freqHz=60 # no need to chang it ever
        print 'ServoDriver START:  freq=%d Hz' % pwm_freqHz
        # Initialise the PWM device using the default address
        rpiDrv = getGPIO()
        dumpPIGIOver(rpiDrv)
        i2cDrv=I2c_for_Adafruit(rpiDrv)
        # Initialise the PCA9685 using the default address (0x40).
        pwmDrv = PCA9685(i2c=i2cDrv)
        i2c_hn=i2cDrv.get_i2c_handle()
        print "init PCA9685, i2c handle=%d"%i2c_hn
        if i2c_hn>25:
            print "%d  open sockets with GPIO daemon - it is not good"%i2c_hn

        # Set frequency to 60hz, good for servos.
        pwmDrv.set_pwm_freq(pwm_freqHz)
        self.pwmDriver=pwmDrv        
        self.i2cDrv=i2cDrv
        self.rpiDrv=rpiDrv

    def linkController(self,first):
        self.pwmDriver=first.pwmDriver

    def config(self,confDi):
        self.myPin=confDi['chan'] # assign pins on 16-servo controller    
        self.pwmZero=confDi['pwmZero'] 
        self.pwm2rad=confDi['pwm2rad'] 
        self.pwmLow=confDi['pwmLow'] 
        self.pwmHigh=confDi['pwmHigh'] 
        self.signFactor=1
        if confDi['inverted']:
            self.signFactor=-1
        #print 'servo-driver chan=',self.myPin
        self.pwmVal=self.pwmLow


    def rad2Pwm(self,ang):
        pwm=int (self.pwmZero + self.signFactor*ang*self.pwm2rad) # for plastic-gear servo from Parallax
        return pwm


    def setAngleDeg(self,angDeg):
        self.setAngle(angDeg/180.*PI)
        
    def setAngle(self,angRad):        
        newPwm=self.rad2Pwm(angRad)
        self.setPwm(newPwm)

    def setPwm(self,newPwm):     
        if newPwm < self.pwmLow:
            newPwm=self.pwmLow
            if self.signFactor>0:
                self.status='low'
            else : 
                self.status='high'

        elif newPwm > self.pwmHigh:
            newPwm=self.pwmHigh
            if self.signFactor>0:
                self.status='high'
            else:
                self.status='low'
        else:
            self.status='ok'
        
        self.pwmVal=newPwm
        self.pwmDriver.set_pwm(self.myPin, 0, self.pwmVal)
        #print "seMoDrv: new  pwm=%d, status=%s" % ( self.pwmVal,self.status)
       
        
    def disable(self):
        self.pwmDriver.set_pwm(self.myPin, 0, 0)
        print 'disable chan=',self.myPin

    def fullStop(self):
        print "all Servos STOP & RELAX, I2C stop, rpiDrv STOP"
        self.i2cDrv.softwareReset()
        self.i2cDrv.close()
        self.rpiDrv.stop()
 
  
